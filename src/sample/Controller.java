package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;

public class Controller {


    @FXML
    BarChart<String, Number> gistoGramm;

    @FXML
    private TextField aValue, mValue, rValue, nValue, intervalsCount;

    private int a, m, r, n, iCount;

    private ArrayList<Float> row = new ArrayList<>();


    private ArrayList<Float> req(int a, int m, int n, int r0) {
        System.out.println("@ ПОЛУЧЕНИЕ СЛУЧАЙНЫХ ЧИСЕЛ----------------------------------------------");
        ArrayList<Float> res = new ArrayList<>();

        int aR = a * r0;
        int rN = aR % m;
        float xN = (float) rN / m;
        res.add(xN);
        System.out.println("n: " + 0 + "    Rn-1: " + r0 + "   aR: " + aR + "   rN: " + rN + "   xN: " + xN);

        for (int i = 1; i < n; i++) {
            aR = a * rN;
            System.out.print("n: " + i + "    Rn-1: " + rN + "   aR: " + aR);
            rN = aR % m;
            xN = (float) rN / m;
            System.out.println("   rN: " + rN + "   xN: " + xN);
            res.add(xN);
        }

        return res;
    }



    private void calcMDN(ArrayList<Float> row) {
        System.out.println("@ расчет математического ожидания, дисперсии и среднего квадратичного отклонения----------------------------------------------");

        // math wait---------------------------------------------------------------------
        float M = 0;
        for (Float xN : row) {
            M += xN;
        }
        M /= row.size();
        System.out.println("! Мат ожидание : " + M);

        // Dispersion------------------------------------------------------------------------
        float D = 0;
        for (Float xN : row) {
            D += (xN - M) * (xN - M);
        }
        D /= row.size();
        System.out.println("! Дисперсия: " + D);

        // otkl--------------------------------------------------------------------------------
        double ny = Math.sqrt(D);
        System.out.println("! Средне квадратичное отклонение: " + ny);
    }



    private void cosvCheck() {

        System.out.println("@ Проверка равномерности по косвенным признакам----------------------------------------------");

        int K = 0;
        for (int i = 0; i < row.size() / 2; i++) {

            Float first = row.get(2 * i);
            Float second = row.get(2 * i + 1);
            Float res = first * first + second * second;
            if (res < 1) {
                K++;
            }
            System.out.println("(" + first + "^2 + " + second + "^2) = " + res);
        }
        System.out.println("! число пар K: " + K);
        double KN = (double) K * 2 / (double) this.n;
        double p4 = 3.14 / 4;
        System.out.println("2K / N: " + KN + " -> " + p4);
    }



    private void gist() {
        System.out.println("@ гистограмма----------------------------------------------");

        float xMax = 0f;
        float xMin = 99f;
        for (Float xN : row) {
            if (xN > xMax) {
                xMax = xN;
            }
            if (xN < xMin) {
                xMin = xN;
            }
        }

        System.out.println("! минимальное : " + xMax + "    максимальное : " + xMin);
        float rVar = xMax - xMin;
        System.out.println("! диапазон варьирования: " + rVar);
        float intervalLength = rVar / (float) iCount;
        System.out.println("! длина интервала: " + intervalLength);


        int[] pointsInIntervalCount = new int[iCount];
        for (float point : row) {
            for (int intervalNumber = 0; intervalNumber < iCount; intervalNumber++) {
                if ((point >= xMin + intervalNumber * intervalLength) && (point <= xMin + (intervalNumber + 1) * intervalLength)) {
                    pointsInIntervalCount[intervalNumber]++;
                    break;
                }
            }
        }

        gistoGramm.getData().clear();

        XYChart.Series<String, Number> dataSeries1 = new XYChart.Series<String, Number>();
        dataSeries1.setName("Гистограмма");
        for (int i = 0; i < iCount; i++) {
            float value = pointsInIntervalCount[i] / (float) n;
            System.out.println(i + ": " + pointsInIntervalCount[i] + " " + value);
            dataSeries1.getData().add(new XYChart.Data<String, Number>(String.valueOf(i), value));
        }
        gistoGramm.getData().add(dataSeries1);
    }




    private void lengthAandP(){

        System.out.println("@ длина периода Р и длина отрезка апериодичности L----------------------------------------------");

        Float xV = row.get(row.size() - 1);
        int periodLength = 0;
        boolean startCountFlag = false;
        for (int i = 0; i < row.size(); i++) {
            if (Objects.equals(row.get(i), xV)) {
                if (startCountFlag) {
                    periodLength = i - periodLength;
                    break;
                } else {
                    periodLength = i;
                    startCountFlag = true;
                }

            }
        }
        System.out.println("! длина периода последовательности Р: " + periodLength);

        int L = 0;
        for (int i = 0; i < row.size(); i++) {
            if ((i + periodLength) < row.size()) {
                if (row.get(i).equals(row.get(i + periodLength))) {
                    L = i + periodLength;
                    break;
                }
            }
        }
        System.out.println("! длинна отрезка апериодичности L: " + L);
    }




    @FXML
    public void onClickGoBtn() {

        row.clear();

        a = Integer.parseInt(this.aValue.getText());
        m = Integer.parseInt(this.mValue.getText());
        r = Integer.parseInt(this.rValue.getText());
        n = Integer.parseInt(this.nValue.getText());
        iCount = Integer.parseInt(this.intervalsCount.getText());

        this.row = this.req(a, m, n, r);

        // мат ожидание, дисперсия, средне квадратичное отклонение
        System.out.println();
        this.calcMDN(row);
        System.out.println();

        //  Проверка равномерности по косвенным признакам--------------------------------------
        System.out.println();
        this.cosvCheck();
        System.out.println();

        // Проверка по гистограмме------------------------------------------------------------
        System.out.println();
        this.gist();
        System.out.println();

        //длина периода Р и длина отрезка апериодичности L--------------------------------------
        System.out.println();
        this.lengthAandP();
        System.out.println();
    }


}
